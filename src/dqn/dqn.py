import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.nn.utils import clip_grad_norm
import torch.optim as optim
import numpy as np
from collections import OrderedDict

use_cuda = torch.cuda.is_available()
#dtype = torch.cuda.FloatTensor if use_cuda else torch.FloatTensor
#dlongtype = torch.cuda.LongTensor if use_cuda else torch.LongTensor

dtype = torch.FloatTensor
dlongtype = torch.LongTensor

class ffn(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, activation, output_activation, dueling, noisy):
        super(ffn, self).__init__()
        
        try:
            iterator = iter(hidden_size)
            self.is_iter = True
        except:
            self.is_iter = False
        
        self.input_size = input_size
        self.output_size = output_size
        self.activation = activation()
        if not output_activation is None:
            self.output_activation = output_activation()
        else:
            self.output_activation = None
        self.dueling = dueling

        self.inputs = nn.Linear(input_size, hidden_size[0])
        self.hidden = OrderedDict([('inputActivation', activation())])
        for i, _ in enumerate(hidden_size[:-1]):
            self.hidden['hidden' + str(i+1)] = nn.Linear(hidden_size[i], hidden_size[i + 1])
        
        if dueling:
            self.advantage = nn.Linear(hidden_size[-1], output_size)
            self.value = nn.Linear(hidden_size[-1], 1)
        else:
            self.outputs = nn.Linear(hidden_size[-1], output_size)
        
    def forward(self, x):
        
        if len(x.size()) == 1:
            x = x.unsqueeze(0)
            batch_size = 1
        else:
            batch_size = x.size(0)
           
        x = self.activation(self.inputs(x))
        for k in self.hidden:
            x = self.activation(self.hidden[k](x))
        if self.dueling:
            adv = self.activation(self.advantage(x))
            val = self.activation(self.value(x)).expand(batch_size, self.output_size)
            x = val + adv - adv.mean(1).unsqueeze(1).expand(batch_size, self.output_size)
        else:
            x = self.outputs(x)
        
        if not self.output_activation is None:
            x = self.output_activation(x)
        return x

class DQN(nn.Module):

    def __init__(self, input_size, hidden_size, output_size, activation = nn.ReLU, output_activation = None, double = True, dueling = True, noisy = True, force_cpu=False):
        super(DQN, self).__init__()
        
        self.force_cpu = force_cpu
        
        # Build model
        self.model = ffn(
            input_size=input_size,
            hidden_size=hidden_size,
            output_size=output_size,
            activation=activation,
            output_activation=output_activation,
            dueling=dueling,
            noisy=noisy
            )
        
        # Target model
        if double:
            self.target_model = ffn(
                input_size=input_size,
                hidden_size=hidden_size,
                output_size=output_size,
                activation=activation,
                output_activation=output_activation,
                dueling=dueling,
                noisy=noisy
            )
            self.target_model.load_state_dict(self.model.state_dict())
        self._double = double
        
        # Hyper Parameters
        self.gamma = 0.9
        self.reg_l2 = 1e-3
        self.max_norm = 1
        self.target_update_period = 100
        lr = 0.001

        self.optimizer = optim.RMSprop(self.model.parameters(), lr=lr)
        self.batch_count = 0

        # To GPU if available
        if use_cuda and not force_cpu:
            self.cuda()
    
    def update_target(self):
        self.target_model.load_state_dict(self.model.state_dict())

    def Variable(self, x):
        if use_cuda and not self.force_cpu:
            return Variable(x, requires_grad=False).cuda()
        else:
            return Variable(x, requires_grad=False)

    def singleBatch(self, batch):
        
        # Reset grads and loss
        self.optimizer.zero_grad()
        loss = 0

        # Each example in a batch: [s, a, r, s_prime, done]
        s = self.Variable(dtype(batch[0]))
        a = self.Variable(dlongtype([[batch[1]]]))
        r = self.Variable(dtype([batch[2]]))
        s_prime = self.Variable(dtype(batch[3]))
        
        q = self.model(s)
        if self._double:
            q_prime = self.target_model(s_prime)
        else:
            q_prime = self.model(s_prime)

        # the batch style of (td_error = r + self.gamma * torch.max(q_prime) - q[a])
        td_error = r.squeeze_(0) + torch.mul(torch.max(q_prime, 1)[0], self.gamma).unsqueeze(1) - torch.gather(q, 1, a)
        loss += td_error.pow(2).sum()

        loss.backward()
        clip_grad_norm(self.model.parameters(), self.max_norm)
        self.optimizer.step()

    def predict(self, inputs):
        inputs = self.Variable(torch.from_numpy(inputs).float())
        if self._double:
            return self.target_model(inputs).cpu().data.numpy()[0]
        else:
            return self.model(inputs).cpu().data.numpy()[0]

    def save_model(self, model_path):
        torch.save(self.model.state_dict(), model_path)
        print("model saved.")

    def load_model(self, model_path):
        self.model.load_state_dict(torch.load(model_path))
        print("model loaded.")