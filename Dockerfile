FROM pytorch/pytorch:latest

SHELL ["/bin/bash", "-c"]

COPY settings/ /opt/settings/

ARG PROXY=http://rd-srv-proxy.priv.atos.fr:3128
ENV http_proxy $PROXY
ENV https_proxy $PROXY

RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y vim curl bzip2 dos2unix patch opengl-python

RUN pip install -r /opt/settings/requirements.txt

#RUN jupyter notebook --generate-config && \
#    cp -r /opt/settings/jupyter_notebook_config.py ~/.jupyter/

ENTRYPOINT ["/bin/bash"]

